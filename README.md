# Oqtut

A nanohub project providing tutorial materials for Infleqtion's Oqtant program [https://oqtant.infleqtion.com](https://oqtant.infleqtion.com).

Project was originally composed for the 2024 Purdue summer school for the Quantum Science Center (QSC) by Noah Fitch.
The 'oqtut.ipynb' notebook at the highest level should be used for nanohub.  Specifically note the non-standard 
user authorization process required on this platform.  Other Oqtant walkthroughs and demonstrations can be found in the examples/ folder.

The most up to date Oqtant examples and documentation can be found here: [https://oqtant-docs.infleqtion.com](https://oqtant-docs.infleqtion.com).