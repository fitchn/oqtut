{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Walkthrough 5: Experimentation and batch jobs \n",
    "This notebook runs on Oqtant hardware and uses **4 jobs** "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction ##"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Multiple jobs are needed to either average results or to scan control parameters to map out dependencies and trends.  In this walkthrough, we will explore ways of submitting and managing lists of *QuantumMatter* objects.  \n",
    "\n",
    "\n",
    "This, along with all our example notebooks are publicly available for download from our [GitLab repository.](https://gitlab.com/infleqtion/albert/oqtant-documentation/-/tree/main/oqtant_documentation/docs/examples?ref_type=heads)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Imports and user authentication ###"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "from oqtant.schemas.quantum_matter import QuantumMatterFactory\n",
    "\n",
    "qmf = QuantumMatterFactory()\n",
    "qmf.get_login()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "qmf.get_client()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Submit a list of QuantumMatter objects to generate many independent jobs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create a list of QuantumMatter objects ###"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Make a list of *QuantumMatter* objects that each have a different target temperature:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "N = 2\n",
    "matters = [\n",
    "    qmf.create_quantum_matter(\n",
    "        temperature=50 * (n + 1), name=\"quantum matter run \" + str(n + 1) + \"/\" + str(N)\n",
    "    )\n",
    "    for n in range(N)\n",
    "]\n",
    "\n",
    "list(map(type, matters))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Submit the list to Oqtant QMS ###"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Submit many independent jobs to be run on the hardware. Each of these jobs enters Oqtant's queueing system at nearly the same time, so they will *likely* be executed near each other in time, depending on current queue usage."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "[matter.submit(track=True) for matter in matters]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Access job results ###"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once all our submitted jobs are complete, we access the results in the same way as if we had submitted the programs individually:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# retrieve jobs from server one at a time, creating corresponding local job objects\n",
    "[matter.get_result() for matter in matters]\n",
    "\n",
    "# access the results individually and plot them together\n",
    "lns = []\n",
    "lbls = []\n",
    "plt.figure(figsize=(5, 4))\n",
    "for matter in matters:\n",
    "    (ln,) = plt.plot(matter.output.get_slice(axis=\"x\"))\n",
    "    plt.xlabel(\"x position (pixels)\")\n",
    "    plt.ylabel(\"OD\")\n",
    "    lns.append(ln)\n",
    "    lbls.append(str(matter.output.temperature_nk))\n",
    "plt.legend(lns, lbls, loc=\"upper right\", title=\"temp (nK)\")\n",
    "plt.show()\n",
    "\n",
    "plt.figure(figsize=(5, 4))\n",
    "plt.plot(\n",
    "    [matter.output.temperature_nk for matter in matters],\n",
    "    [\n",
    "        (\n",
    "            100 * matter.output.condensed_atom_number / matter.output.tof_atom_number\n",
    "            if matter.output.tof_atom_number > 0\n",
    "            else 0\n",
    "        )\n",
    "        for matter in matters\n",
    "    ],\n",
    "    \"-o\",\n",
    ")\n",
    "plt.xlabel(\"temperature (nK)\")\n",
    "plt.ylabel(\"condensed fraction (%)\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Generate and submit a \"batch\" job ##"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is also the option to submit a list of *matter* objects as a single *batch* job, which guarantees that the sequence executes sequentially on the hardware.  This feature is useful for detailed experimentation or investigation, where subsequent shots need to be compared to each other in detail.  Using sequential hardware shots reduces system drift or inconsistency.  \n",
    "\n",
    "In the case of bundling into a single batch job, only one job id will be generated.  Programmatically, the batch job will be composed of multiple *run*s on Oqtant hardware, and retrieving job results will require specifying the run number $1 \\ldots N$ when fetching the job results, where there were $N$ runs in the job.    \n",
    "\n",
    "*NOTE: The resulting name of a batch job will default to the name given to the first QuantumMatter object in the provided list.  Alternatively, a global name can be provided at the point of submission to the client.*  \n",
    "\n",
    "*NOTE: Each individual *run* is charged against your job quota.  A single batch job will naturally contain multiple runs.*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create a list of QuantumMatter objects to submit as a batch ###"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create a list of QuantumMatter objects\n",
    "N = 2\n",
    "matters = [\n",
    "    qmf.create_quantum_matter(\n",
    "        temperature=50 * (n + 1),\n",
    "        lifetime=20 + (n * 2),\n",
    "        time_of_flight=3 + (n * 2),\n",
    "        note=f\"{n + 1}/{N}\",  # notes persist in batches and will remain tied to each matter object\n",
    "    )\n",
    "    for n in range(N)\n",
    "]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Submit the list as a batch job ###"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Submit our list of *matter* objects to generate a batch job using the `QuantumMatterFactory.submit_list_as_batch()` method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# submit the list as a batch that will run sequentially on the hardware\n",
    "# returns only a single job id, e.g., \"1cdb4ff7-c5ed-46d3-a667-8b12f0cd1349\"\n",
    "matter_batch = qmf.submit_list_as_batch(\n",
    "    matter_list=matters, name=\"a batch!\", track=True  # global batch name\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Access batch job results ###"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Retrieve batch job run results for each run by specifying the desired run number using the `QuantumMatter.get_result()` method.  If omitted (as for non-batch jobs with just a single run), the data for the first run will be fetched.  \n",
    "\n",
    "*NOTE: The added complication of managing multiple runs within a single job is is an unavoidable consequence of ensuring that the runs execute sequentially on the hardware.*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "matter_batch.get_result()  # by default this matter object will be set to the first run (input/output values)\n",
    "print(f\"Run: {matter_batch.run} of {len(matters)}\")\n",
    "print(f\"Job Name: {matter_batch.name}\")\n",
    "print(f\"Job Note: {matter_batch.note}\")\n",
    "print(f\"Input Values: {matter_batch.input}\")\n",
    "print(f\"Temperature: {matter_batch.output.temperature_nk}\")\n",
    "\n",
    "print(\"\\n\")\n",
    "\n",
    "matter_batch.get_result(\n",
    "    run=2\n",
    ")  # the matter object will now be set to the second run (input/output values)\n",
    "print(f\"Run: {matter_batch.run} of {len(matters)}\")\n",
    "print(f\"Job Name: {matter_batch.name}\")\n",
    "print(f\"Job Note: {matter_batch.note}\")\n",
    "print(f\"Input Values: {matter_batch.input}\")\n",
    "print(f\"Temperature: {matter_batch.output.temperature_nk}\")\n",
    "\n",
    "print(\"\\n\")\n",
    "\n",
    "# the matter object is now set for the second run, subsequent calls to get_result will continue to use data\n",
    "# for the second run until given a different run value\n",
    "matter_batch.get_result()\n",
    "print(f\"Run: {matter_batch.run} of {len(matters)}\")\n",
    "matter_batch.get_result(run=1)\n",
    "print(f\"Run: {matter_batch.run} of {len(matters)}\")\n",
    "\n",
    "# to determine what run a particular matter is using at anytime you can\n",
    "# print(matter_batch.run)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plot the results together as above, but augment our approach at extracting the data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lns = []\n",
    "lbls = []\n",
    "plt.figure(figsize=(5, 4))\n",
    "for run in range(len(matters)):\n",
    "    matter_batch.get_result(run=run + 1)\n",
    "    (ln,) = plt.plot(matter_batch.output.get_slice(axis=\"x\"))\n",
    "    plt.xlabel(\"x position (pixels)\")\n",
    "    plt.ylabel(\"OD\")\n",
    "    lns.append(ln)\n",
    "    lbls.append(str(matter_batch.output.temperature_nk))\n",
    "plt.legend(lns, lbls, loc=\"upper right\", title=\"temp (nK)\")\n",
    "plt.show()\n",
    "\n",
    "temps = []\n",
    "atoms = []\n",
    "plt.figure(figsize=(5, 4))\n",
    "for run in range(len(matters)):\n",
    "    matter_batch.get_result(run=run + 1)\n",
    "    temps.append(matter_batch.output.temperature_nk)\n",
    "    atoms.append(\n",
    "        (\n",
    "            100\n",
    "            * matter_batch.output.condensed_atom_number\n",
    "            / matter_batch.output.tof_atom_number\n",
    "            if matter_batch.output.tof_atom_number > 0\n",
    "            else 0\n",
    "        )\n",
    "    )\n",
    "plt.plot(temps, atoms, \"-o\")\n",
    "plt.xlabel(\"temperature (nK)\")\n",
    "plt.ylabel(\"condensed fraction (%)\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Saving and loading batch job results ###"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When working with jobs that have multiple runs it is important to differenciate between then while saving their data since they share the same job id. By using `QuantumMatter.get_result` we are able to swap the input and output data of a job while persisting it's id.\n",
    "\n",
    "To address this issue the `QuantumMatter.write_job_to_file` method will automatically detect when a job has more than one run and append the appropriate indictor to the end of the file name: _id_run_n_of_N.txt_"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "matter_batch.get_result(run=1)\n",
    "matter_batch.write_to_file()\n",
    "matter_batch.get_result(run=2)\n",
    "matter_batch.write_to_file()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
