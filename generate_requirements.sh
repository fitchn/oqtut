#!/bin/bash

echo generating requirements file
poetry export --without-hashes --without-urls -f requirements.txt -o requirements.txt